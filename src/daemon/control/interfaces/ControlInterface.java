/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package daemon.control.interfaces;

/**
 *
 * @author Hanyuu
 */
public interface ControlInterface {
    
    public String startDaemon();
    public String stopDaemon();
    public String reLoadDaemon();
    public String getLettersCount();
    
}
