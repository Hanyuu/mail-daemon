/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daemon.control.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.HashLoginService;
import org.eclipse.jetty.security.SecurityHandler;
import org.eclipse.jetty.security.authentication.BasicAuthenticator;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.security.Constraint;
import org.eclipse.jetty.util.security.Credential;

import java.io.IOException;
import java.util.logging.Logger;

import daemon.control.interfaces.ControlInterface;
import daemon.config.WebCmds;
import daemon.config.Config;
import daemon.daemon.Daemon;
import daemon.Start;
/**
 *
 * @author Hanyuu
 */
public class WebControl extends HttpServlet implements ControlInterface {

    private static final Logger log = Logger.getLogger(WebControl.class.getCanonicalName());
    private static final long serialVersionUID = 1L;


    @Override
    public void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("application/json;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        String result = "{'error':'no-route','id':"+Config.DAEMON_ID+"}";
        try {
            switch (WebCmds.valueOf(request.getRequestURI().replace("/", ""))) {
                case start:
                    result = startDaemon();
                    response.getWriter().println(result);
                    break;
                case stop:
                    result = stopDaemon();
                    response.getWriter().println(result);
                    break;
                case reload:
                    result = reLoadDaemon();
                    response.getWriter().println(result);
                    break;
                case status:
                    result = getLettersCount();
                    response.getWriter().println(result);
                    break;
                case shutdown:
                    System.exit(-1);
                default:
                    response.getWriter().println(result);
                    break;
            }
            
        } catch (Exception e) {
            response.setContentType("text/html;charset=utf-8");
            response.getWriter().println("<html> "
                    + "<a target='_blank' href='/start/'>Start</a><br/>"
                    + "<a target='_blank' href='/stop/'>Stop</a><br/>"
                    + "<a target='_blank' href='/status/'>Status</a><br/>"
                    + "<a target='_blank' href='/shutdown/'>Shutdown</a><br/>"
                    + "<br/><html>");
        }
    }

    @Override
    public String startDaemon() {
        Daemon d = Daemon.getInstance();

        String status = d.demoniaze(Config.IS_DEBUG);
        if( "Started".equals(status)){
            return "{'status':'ok','id':'"+Config.DAEMON_ID+"', 'daemon-status':'"+d.getStatus()+"'}";
        }else{
           return "{'status':'error','error':'"+status+"','id':'"+Config.DAEMON_ID+"', 'daemon-status':'"+d.getStatus()+"'}"; 
        }
    }

    @Override
    public String stopDaemon() {
        Daemon d = Daemon.getInstance();
        if("Stopped".equals(d.doCancel())){
            return "{'status':'ok':,'id':'"+Config.DAEMON_ID+"', 'daemon-status':'"+d.getStatus()+"'}";
        }else{
            return "{'status':'error','id':'"+Config.DAEMON_ID+"', 'daemon-status':'"+d.getStatus()+"'}";
        }
    }

    @Override
    public String reLoadDaemon() {
        Config.load(Start.cfg);
        Daemon d = Daemon.getInstance();
        return "{'ok':'reloaded','id':'"+Config.DAEMON_ID+"', 'daemon-status':'"+d.getStatus()+"'}";
    }
    
    @Override
    public String getLettersCount(){
        Daemon d = Daemon.getInstance();
        
        return "{'system_letters':'"+d.system_lettes_count+"','letters':'"+d.lettes_count+"','id':'"+Config.DAEMON_ID+"','daemon-status':'"+d.getStatus()+"'}";
    }

    public static void runServer() throws Exception {
        Server server = new Server(Config.WEB_CONTROL_PORT);
        
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setSecurityHandler(basicAuth(Config.WEB_CONTROL_LOGIN, Config.WEB_CONTROL_PASSWORD, "admin"));
        context.setContextPath("/*");
        context.addServlet(new ServletHolder(new WebControl()), "/*");
        server.setHandler(context);
        server.start();
        server.join();
    }

    private static final SecurityHandler basicAuth(String username, String password, String realm) {
        HashLoginService l = new HashLoginService();
        l.putUser(username, Credential.getCredential(password), new String[]{"admin"});
        l.setName(realm);
        Constraint constraint = new Constraint();
        constraint.setName(Constraint.__BASIC_AUTH);
        constraint.setRoles(new String[]{"admin"});
        constraint.setAuthenticate(true);
        ConstraintMapping cm = new ConstraintMapping();
        cm.setConstraint(constraint);
        cm.setPathSpec("/*");
        ConstraintSecurityHandler csh = new ConstraintSecurityHandler();
        csh.setAuthenticator(new BasicAuthenticator());
        csh.setRealmName("myrealm");
        csh.addConstraintMapping(cm);
        csh.setLoginService(l);
        return csh;
    }
}
