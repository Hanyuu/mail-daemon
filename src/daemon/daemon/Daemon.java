/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daemon.daemon;

import daemon.parallel.Parallel;
import daemon.models.DataBase;
import daemon.models.Letter;
import daemon.models.SystemLetter;
import java.util.*;
import java.util.logging.*;
import daemon.config.Config;
import daemon.parallel.SendLetterOperation;
import java.sql.SQLException;
import daemon.daemon.Mail;
/**
 *
 * @author Hanyuu
 */
public class Daemon  {

    private static final Logger log = Logger.getLogger(Daemon.class.getCanonicalName());

    private static Daemon _instance = null;

    public long system_lettes_count = 0L;
    public long lettes_count = 0L;
    
    private boolean isDebug = false;
    
    private Timer timer;
    private String status;
    public static Daemon getInstance() {
        if (_instance == null) {
            _instance = new Daemon();
        }
        return _instance;
    }

    public void startSending() {
        try {
            List<Object> letters = new ArrayList<>();
            // temporarily we dont use system letters
            //letters.addAll(DataBase.getInstance().getSystemLetters());
            letters.addAll(DataBase.getInstance().getLetters());

            Parallel.ForFJ(letters, new SendLetterOperation() );

        } catch (SQLException e) {
            e.printStackTrace();
            log.log(Level.WARNING, "EXCEPTION {0} ", new Object[]{e.getLocalizedMessage()});
        }
    }
    
    public String doCancel(){
        this.timer.cancel();
        this.status = "Stopped";
        return this.status;
    }
    
    public String getStatus(){
        return this.status;
    }
    public String demoniaze(boolean isDebug) {
        try {
            this.isDebug = isDebug;
            if(this.status!="Started"){
                this.timer = new Timer();
                timer.schedule(new DaemonTask(), 0, Config.TIMER_SCHEDULE_PERIOD);
                this.status = "Started";
                }
            
            return this.status;
        } catch (Exception e) {
            e.printStackTrace();
            return e.getLocalizedMessage();
        }
    }

    public static void p(Object o) {
        System.out.println(o);
    }
}
