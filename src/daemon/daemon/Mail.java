/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daemon.daemon;

import com.sun.mail.smtp.SMTPTransport;
import daemon.config.Config;
import daemon.models.SystemLetter;
import daemon.models.Letter;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.google.gson.Gson;
import java.util.Map;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;
import java.util.function.BiConsumer;

/**
 *
 * @author Hanyuu
 */
public class Mail {

    private static final Logger log = Logger.getLogger(Mail.class.getCanonicalName());
    private Session session;
  //  private SMTPTransport transport;
    private static Mail _instance = null;

    public Mail() {
        try {
            boolean debug = false;
            String prot = "smtp";
            Properties props = new Properties();
            props.put((new StringBuilder()).append("mail.").append(prot).append(".auth").toString(), "true");

            this.session = Session.getInstance(props);
            this.session.setDebug(debug);
        //    this.transport = (SMTPTransport) session.getTransport(prot);
        } catch (Exception e) {
            e.printStackTrace();
            this.log.warning("mail "+e.getLocalizedMessage());
            System.exit(-1);
        }
    }

    public static Mail getInstance() {
        if (_instance == null) {
            _instance = new Mail();
        }
        return _instance;
    }

    public boolean buildAndSend(Object l) {
        return this.sendMessage(this.buildMessage(l));
    }

    public Message buildMessage(Object l) {
        if (l instanceof SystemLetter || l instanceof Letter) {

            try {
                SystemLetter letter = ((SystemLetter) l);

                Message msg = new MimeMessage(session);
                msg.setFrom(new InternetAddress(letter.getMailFrom(), letter.getNameFrom()));
      
                msg.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(letter.getMailTo(), false));
                msg.setSubject(letter.getSubject());
                msg.setSentDate(new Date());
                msg.setContent(letter.getBody(), "text/html;charset=UTF-8");
                msg.setHeader("Precedence", "bulk");

                Type stringStringMap = new TypeToken<Map<String, String>>() {
                }.getType();
                Gson gson = new Gson();
                Map<String, String> headers = gson.fromJson(letter.getHeaders(), stringStringMap);

                BiConsumer<String, String> biConsumer = (x, y) -> {
                    try {
                        msg.setHeader(x, y);
                    } catch (Exception e) {
                        e.printStackTrace();
                        log.log(Level.WARNING, "{0}", new Object[]{e});
                    }
                    
                };
                headers.forEach(biConsumer);

                return msg;
            } catch (AddressException ae) {
                ae.printStackTrace();
                return null; //Just delete
            } catch (SendFailedException sfe) {

                sfe.printStackTrace();
                if (sfe.getInvalidAddresses().length > 0) {
                    log.log(Level.WARNING, "{0} {1}", new Object[]{sfe, sfe.getInvalidAddresses()[0]});
                    return null; //Just delete wrong adresses
                } else {
                    log.log(Level.WARNING, "{0}", new Object[]{sfe});
                    return null;
                }
            } catch (Exception mex) {
                log.log(Level.WARNING, "{0}", new Object[]{mex});
                mex.printStackTrace();
                return null;
            }

        }
        return null;
    }

    private boolean sendMessage(Message message) {
        if (message == null) {
            return false;
        }
        try {
            /*
            if (!this.transport.isConnected()) {
                this.transport.connect(Config.SMPT_HOST, Config.SMPT_PORT, Config.SMPT_LOGIN, Config.SMPT_PASSWORD);
            }*/
            
            SMTPTransport transport = (SMTPTransport) session.getTransport("smtp");
            transport.connect(Config.SMPT_HOST, Config.SMPT_PORT, Config.SMPT_LOGIN, Config.SMPT_PASSWORD);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            this.log.warning(e.getLocalizedMessage());
            return false;
        }

    }
}
