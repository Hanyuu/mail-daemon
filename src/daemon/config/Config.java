/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package daemon.config;

/**
 *
 * @author Hanyuu
 */
public class Config extends ConfigAbstract{
    
    public static String DAEMON_ID;
    public static String DATA_BASE_CLASS;
    public static String DATA_BASE_URL;
    public static String DATA_BASE_LOGIN;
    public static String DATA_BASE_PASSWORD;
    
    public static String DATA_BASE_GET_SYSTEM_LETTERS;
    public static String DATA_BASE_GET_LETTERS;
    public static String DATA_BASE_DELETE_SYSTEM_LETTER;
    public static String DATA_BASE_DELETE_LETTER;
    
    public static int    DATA_BASE_MIN_IDLE;
    public static int    DATA_BASE_MAX_IDLE;
    
    public static String SMPT_HOST;
    public static String SMPT_LOGIN;
    public static String SMPT_PASSWORD;
    public static String SMPT_REPLY_TO;
    public static int SMPT_PORT;
    
    public static int WEB_CONTROL_PORT;
    public static String WEB_CONTROL_LOGIN;
    public static String WEB_CONTROL_PASSWORD;
    
    public static int PARALLEL_NUM_PARALLELISM;
    
    public static int TIMER_SCHEDULE_PERIOD;
    
    public static boolean  IS_DEBUG;
    public static boolean  DELETE_LETTERS;
    
    
    
    public static void load(String file){
        try{
            Load(file);
            DAEMON_ID = getString("daemon.id", "1-x");
            
            
            DATA_BASE_CLASS = getString("jdbc.driver", "com.mysql.jdbc.Driver");
            DATA_BASE_URL = getString("jdbc.url","jdbc:mysql://ruelsoft.org:3306/ALTER-MAILSENDER");
            DATA_BASE_LOGIN = getString("jdbc.login", "appsUser");
            DATA_BASE_PASSWORD = getString("jdbc.password", "GjENyfMPbWStJBPs");
            
            DATA_BASE_MIN_IDLE = getInt("jdbc.min.idle", "50");
            DATA_BASE_MAX_IDLE = getInt("jdbc.max.idle", "500");
            
            DATA_BASE_GET_SYSTEM_LETTERS = getString("jdbc.get.system.letters","SELECT * FROM `LettersSystem` WHERE (SendDate IS null OR SendDate <= Now())");
            DATA_BASE_GET_LETTERS = getString("jdbc.get.letters","SELECT * FROM `Letters` WHERE (SendDate IS null OR (SendDate <= Now() AND SendDate <> '0000-00-00 00:00:00')) AND StatusMail <>3  ORDER BY StatusMail ASC, SendDate LIMIT 0,5000");
            DATA_BASE_DELETE_LETTER = getString("jdbc.delete.letter","DELETE FROM `Letters` WHERE id = ?");
            DATA_BASE_DELETE_SYSTEM_LETTER = getString("jdbc.delete.system.letter","DELETE FROM `LettersSystem` WHERE id = ?");
            
            SMPT_HOST = getString("smpt.host", "localhost");
            SMPT_PORT = getInt("smpt.port", "25");
            SMPT_LOGIN = getString("smpt.login", "responder");
            SMPT_PASSWORD = getString("smpt.password", "xR88jt4dmf");
            SMPT_REPLY_TO = getString("smpt.reply.to", "no-reply@ruelsoft.org");
            
            WEB_CONTROL_PORT = getInt("web.control.port","8800");
            WEB_CONTROL_LOGIN = getString("web.control.login","sard");
            WEB_CONTROL_PASSWORD = getString("web.control.password","sard21");
            
            PARALLEL_NUM_PARALLELISM = getInt("parallel.parallelism","32");
            
            TIMER_SCHEDULE_PERIOD = getInt("timer.schedule.period","2000");
            
            IS_DEBUG = getBoolean("daemon.debug", "false");
            DELETE_LETTERS = getBoolean("daemon.delete_letters","false");
            
            
            
        }catch(Exception e){
            e.printStackTrace();
            System.exit(-1);
        }
    }
    
}
