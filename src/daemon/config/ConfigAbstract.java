
package daemon.config;

import java.io.File;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.util.Properties;


public class ConfigAbstract 
{

    private static final Properties p = new Properties();
    private static String _file;
    public static void setFile(String file)
    {
        _file=file;
    }
    public static void Load(String file)
    {
        _file = file;
        File f=new File(_file);
        System.err.println(f.getAbsolutePath());
        try
        {
            if(!f.exists())
            {
                System.out.println("Cfg file not found!\nUSE DEFAUL VALUES");
                
                f.createNewFile();
            }
            else
            {
                BufferedReader r=new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));
                p.load(r);
                r.close();
            }
        }
        catch(Exception e)
        {
            System.out.println("error while loading");
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static boolean getBoolean(String value,String dvalue)
    {
        return Boolean.parseBoolean(getString(value,dvalue));
    }
    public static int getInt(String value,String dvalue)
    {
        return Integer.parseInt(getString(value,dvalue));
    }
    public static long getLong(String value,String dvalue)
    {
        return Long.parseLong(getString(value,dvalue));
    }
    public static String getString(String value,String dvalue)
    {
        return p.getProperty(value,dvalue).trim();
    }
    public static void saveConfigParam(String key, String value)
    {
        p.setProperty(key, value);
        store();
    }
    public static void saveConfigParam(String key, boolean value)
    {
        saveConfigParam(key, String.valueOf(value));
    }
    public static void saveConfigParam(String key, int value)
    {
        saveConfigParam(key, String.valueOf(value));
    }
    public static void saveConfigParam(String key, double value)
    {
        saveConfigParam(key, String.valueOf(value));
    }
    public static void saveConfigParam(String key, long value)
    {
        saveConfigParam(key, String.valueOf(value));
    }

    public static void store()
    {
        try
        {
            BufferedWriter o=new BufferedWriter(new OutputStreamWriter(new FileOutputStream(_file),"UTF-8"));
            p.store(o,"");
            o.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

}
