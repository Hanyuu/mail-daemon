/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daemon;

import daemon.daemon.Daemon;
import daemon.redis.Redis;
import daemon.models.DataBase;
import static daemon.daemon.Daemon.p;
import daemon.config.Cmds;
import java.io.File;
import java.util.logging.LogManager;
import daemon.control.web.WebControl;
import daemon.config.Config;
import daemon.daemon.Mail;

/**
 *
 * @author Hanyuu
 */
public class Start {

    public static String cfg = "./conf/production.ini";

    public static void runWeb() {
        Thread t = new Thread("WEB-INTERFACE") {
            @Override
            public void run() {
                try {
                    WebControl.runServer();
                    p("WEB INTERFACE STARTED PORT IS:"+Config.WEB_CONTROL_PORT);
                } catch (Exception e) {
                    p("WEB INTERFACE NOT STARTED");

                }
            }
        };
        t.start();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        File logs = new File("./logs");
        if (!logs.exists()) {
            logs.mkdir();
        }else{
           for (String s:logs.list()){
               File log = new File(logs.getAbsolutePath()+"/"+s);
               log.delete();
           }
        }

        if (args.length == 0) {
            p("You must use 'java -jar daemon.jar start {config file}|test|testdb|testredis'");
            System.exit(0);
        } else {
            if (args.length > 1) {
                cfg = args[1];
            }
            Config.load(cfg);
            Daemon d = Daemon.getInstance();
            runWeb();
            switch (Cmds.valueOf(args[0])) {
                case start:
                    Process p = Runtime.getRuntime().exec("java -jar daemon.jar daemon ");
                    p("Daemon started");
                    System.exit(0);
                    break;
                case daemon:
                    LogManager.getLogManager().readConfiguration(Daemon.class.getResourceAsStream("/logging.properties"));
                    d.demoniaze(Config.IS_DEBUG);
                    break;
                case test:
                    Mail.getInstance();
                    break;
                case testdb:
                    //p(DataBase.getInstance().getSystemLetters());
                    p(DataBase.getInstance().getLetters());
                    break;
                case testredis:
                    new Redis();
                    break;                  

            }
        }
    }
}
