/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daemon.parallel;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.logging.Level;
import java.util.logging.Logger;
import daemon.config.Config;


public class Parallel {
 
    private static final ExecutorService forPool = Executors.newFixedThreadPool(Config.PARALLEL_NUM_PARALLELISM, new NamedThreadFactory("Parallel.For"));
    private static final ForkJoinPool fjPool = new ForkJoinPool(Config.PARALLEL_NUM_PARALLELISM, ForkJoinPool.defaultForkJoinWorkerThreadFactory, null, false);

    public static <T> void For(final Iterable<T> elements, final Operation<T> operation) {
        for (final T element : elements) {
            forPool.submit(() -> {
                try {
                    operation.perform(element);
                } catch (Exception e) {
                    Logger.getLogger(Parallel.class.getName()).log(Level.SEVERE, "Exception during execution of parallel task", e);
                }
                return null;
            });
        }
    }

    public static <T> void ForFJ(final Iterable<T> elements, final Operation<T> operation) {
        // TODO: is this really utilizing any fork-join capabilities since it is just an invokeAll?
        // I assume work stealing is at least going on since this is sumbitted to a fork-join pool?
        // but performance tests don't show a different between this and the old way.
        fjPool.invokeAll(createCallables(elements, operation));
    }

    public static <T> Collection<Callable<Void>> createCallables(final Iterable<T> elements, final Operation<T> operation) {
        List<Callable<Void>> callables = new LinkedList<>();
        for (final T elem : elements) {
            callables.add((Callable<Void>) () -> {
                operation.perform(elem);
                return null;
            });
        }

        return callables;
    }

}