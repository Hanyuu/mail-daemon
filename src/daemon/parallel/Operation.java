/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package daemon.parallel;

/**
 *
 * @author Hanyuu
 * @param <T>
 */
public interface Operation<T> {

    public void perform(T pParameter);
    
}
