/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daemon.parallel;

import daemon.config.Config;
import daemon.daemon.Mail;
import daemon.models.SystemLetter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Furude
 */
public class SendLetterOperation implements Operation {

    private static final Logger log = Logger.getLogger(SendLetterOperation.class.getCanonicalName());
    @Override
    public void perform(Object letter) {
        try {
            Mail mail = new Mail();
            if (mail.buildAndSend(letter)) {
                if(Config.DELETE_LETTERS){
                    ((SystemLetter) letter).delete();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            log.log(Level.WARNING, "EXCEPTION {0} ", new Object[]{e.getLocalizedMessage()});
        }
    }

}
