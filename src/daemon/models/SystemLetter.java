/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daemon.models;

import java.sql.SQLException;
import java.util.Date;

/**
 *
 * @author Hanyuu
 */
public class SystemLetter {
    protected int id;
         
    private String nameFrom;
    
    private String mailFrom;
    
    private String mailTo;
    
    private String subject;
    
    private String body;
    
    private String headers;
    
    private Date sendDate;
    
    private Date createdDate;
    
    //UserID 	fromName 	fromMail 	toMail 	Subject 	bodyMail 	CreateDate 	StatusMail 	SendDate
    
    public void setId(int id){
        this.id=id;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }
    
    
    @Override
    public String toString(){
        return "["+this.getId()+"|"+this.getMailFrom()+"|"+this.getMailTo()+"]";
    }
    
    public void delete() throws SQLException{
        DataBase.getInstance().deleteSystemLetter(this);
    }

    /**
     * @return the nameFrom
     */
    public String getNameFrom() {
        return nameFrom;
    }

    /**
     * @param nameFrom the nameFrom to set
     */
    public void setNameFrom(String nameFrom) {
        this.nameFrom = nameFrom;
    }

    /**
     * @return the mailFrom
     */
    public String getMailFrom() {
        return mailFrom;
    }

    /**
     * @param mailFrom the mailFrom to set
     */
    public void setMailFrom(String mailFrom) {
        this.mailFrom = mailFrom;
    }

    /**
     * @return the mailTo
     */
    public String getMailTo() {
        return mailTo;
    }

    /**
     * @param mailTo the mailTo to set
     */
    public void setMailTo(String mailTo) {
        this.mailTo = mailTo;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the body
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body the body to set
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * @return the sendDate
     */
    public Date getSendDate() {
        return sendDate;
    }

    /**
     * @param sendDate the sendDate to set
     */
    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    /**
     * @return the createdDate
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the headers
     */
    public String getHeaders() {
        return headers;
    }

    /**
     * @param headers the headers to set
     */
    public void setHeaders(String headers) {
        this.headers = headers;
    }
}
