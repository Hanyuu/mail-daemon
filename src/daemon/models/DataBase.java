/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daemon.models;

import daemon.models.Letter;
import daemon.models.SystemLetter;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import java.util.AbstractList;
import daemon.config.Config;

/**
 *
 * @author Hanyuu
 */
public class DataBase {

    private static final Logger log = Logger.getLogger(DataBase.class.getCanonicalName());
    private static DataBase instance = null;
    private BasicDataSource dataSource;

    public static DataBase ReLoad(){
        instance=null;
        return getInstance();
    }
    public static DataBase getInstance() {
        if (instance == null) {
            instance = new DataBase();
        }
        return instance;
    }

    public DataBase() {
        this.dataSource = new BasicDataSource();
        this.dataSource.setDriverClassName(Config.DATA_BASE_CLASS);
        this.dataSource.setUrl(Config.DATA_BASE_URL);
        this.dataSource.setUsername(Config.DATA_BASE_LOGIN);
        this.dataSource.setPassword(Config.DATA_BASE_PASSWORD);
        this.dataSource.setMinIdle(Config.DATA_BASE_MIN_IDLE);
        this.dataSource.setMaxIdle(Config.DATA_BASE_MAX_IDLE);
    }

    public Connection getConnection() {
        try {
            return this.dataSource.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
            log.log(Level.WARNING, "EXCEPTION {0} ", new Object[]{e.getLocalizedMessage()});
            return null;
        }
    }

    public List<SystemLetter> getSystemLetters() throws SQLException{
        QueryRunner run = new QueryRunner(this.dataSource);

        ResultSetHandler<List<SystemLetter>> h = new BeanListHandler<SystemLetter>(SystemLetter.class);
        return run.query(Config.DATA_BASE_GET_SYSTEM_LETTERS, h);

    }
    
    public List<Letter> getLetters() throws SQLException{
        QueryRunner run = new QueryRunner(this.dataSource);

        ResultSetHandler<List<Letter>> h = new BeanListHandler<Letter>(Letter.class);

        return run.query(Config.DATA_BASE_GET_LETTERS, h);

    }
    
    public void deleteLetter(Letter letter)throws SQLException{
        QueryRunner run = new QueryRunner(this.dataSource);
        run.update(Config.DATA_BASE_DELETE_LETTER, letter.getId());
       
    }
    
    public void deleteSystemLetter(SystemLetter letter)throws SQLException{
        QueryRunner run = new QueryRunner(this.dataSource);
        run.update(Config.DATA_BASE_DELETE_SYSTEM_LETTER, letter.getId());
    }
}
