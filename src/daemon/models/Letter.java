/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daemon.models;

import java.sql.SQLException;
import java.util.Date;

/**
 *
 * @author Hanyuu
 */
public class Letter extends SystemLetter{

    private int author_id;
    
    private int subscription_id;
    
    private int series_id;
    
    private int status;
    
    private int isFrozen;
    
    private int priority;
    

    //UserID 	fromName 	fromMail 	toMail 	Subject 	bodyMail 	CreateDate 	StatusMail 	SendDate
    


    
    @Override
    public String toString(){
        return "["+this.getId()+"|"+this.getAuthor_id()+"|"+this.getMailFrom()+"|"+this.getMailTo()+"|"+this.getSendDate()+"|"+this.getStatus()+"]";
        }
    
    @Override
    public void delete() throws SQLException{
        DataBase.getInstance().deleteLetter(this);
    }

    /**
     * @return the author_id
     */
    public int getAuthor_id() {
        return author_id;
    }

    /**
     * @param author_id the author_id to set
     */
    public void setAuthor_id(int author_id) {
        this.author_id = author_id;
    }

    /**
     * @return the subscription_id
     */
    public int getSubscription_id() {
        return subscription_id;
    }

    /**
     * @param subscription_id the subscription_id to set
     */
    public void setSubscription_id(int subscription_id) {
        this.subscription_id = subscription_id;
    }

    /**
     * @return the series_id
     */
    public int getSeries_id() {
        return series_id;
    }

    /**
     * @param series_id the series_id to set
     */
    public void setSeries_id(int series_id) {
        this.series_id = series_id;
    }



    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the isFrozen
     */
    public int getIsFrozen() {
        return isFrozen;
    }

    /**
     * @param isFrozen the isFrozen to set
     */
    public void setIsFrozen(int isFrozen) {
        this.isFrozen = isFrozen;
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }
}
